﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class JointController : MonoBehaviour {


	[SerializeField]
	private GameObject _leftKnee;
	[SerializeField]
	private GameObject _rightKnee;
	[SerializeField]
	private GameObject _spineBase;
	[SerializeField]
	private GameObject _spineShoulder;
	[SerializeField]
	private GameObject _bodyObj;

	private Vector3 spineBase;
	private Vector3 spineShoulder;
	private Vector3 leftKnee;
	private Vector3 rightKnee;
	private Color signalColor;
	private Color col1;
	private Color col2;
	// Use this for initialization
	void Start () {
		signalColor = new Color (204f,94f,75f,10f);
		col1 = _leftKnee.GetComponent<Renderer> ().material.color;
		col2 = _rightKnee.GetComponent<Renderer> ().material.color;
	}
	
	public void JointUpdate(Vector3 rightKnee, Vector3 leftKnee,Vector3 spineBase, Vector3 spineShoulder){
		
		this.rightKnee=rightKnee;
		this.leftKnee = leftKnee;
		_spineBase.transform.position = spineBase;
		_spineShoulder.transform.position = spineShoulder;

		KneeJointsUpdate (rightKnee, leftKnee);
		SpineJointsUpdate (_spineBase, _spineShoulder);
	}

	void KneeJointsUpdate(Vector3 rKnee, Vector3 lKnee){
		if (Vector3.Distance (rKnee, lKnee) > .2f) {
			_leftKnee.SetActive (true);
			_rightKnee.SetActive (true);
		} else {
			_leftKnee.SetActive (false);
			_rightKnee.SetActive (false);
		}
	}

	void SpineJointsUpdate(GameObject sBase, GameObject shoulder){		
		Vector3 direction = sBase.transform.position - shoulder.transform.position;
		float sidewaysAngle=Vector3.Angle(direction,sBase.transform.right);
		float backfrontAngle = Vector3.Angle (direction,sBase.transform.forward);
		Debug.Log (backfrontAngle.ToString());
		if (sidewaysAngle < 85f || sidewaysAngle > 95f || backfrontAngle > 100f || backfrontAngle < 80f) {
			_bodyObj.SetActive (true);
		} else {
			_bodyObj.SetActive (false);
		}

	}

}
